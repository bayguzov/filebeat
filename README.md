# Ansible Role: filebeat

Set up and configure filebeat log shipper

## Role Variables

```yaml
filebeat_config:
  filebeat:
    logstash_srv_address: ""
    logstash_srv_port: 5044
    files_under_root: true
    output:
      logstash:
        hosts: []
    ssl:
      enabled: false
      certificate_authorities: []
    conf_d: []
    modules_d: []
```

## output.logstash.hosts

List of "host:port" strings

## ssl.certificate_authorities

List of CA files

```yaml
- path: /etc/filebeat/filebeatCA.crt
  content: ""
```

## conf.d

list of conf.d files
```yaml
- name: phpd
  paths: ["/var/log/phpdaemon.log"]
  multiline: true
  fields:
    tags: phpd_error
```

## modules.d
list of modules.d files

For more information see <https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-modules.html>

```yaml
    modules_d:
      nginx:
        access:
          enabled: "false"
        error:
          enabled: "true"
```

# Example playbook

```yaml
    filebeat_config:
      filebeat:
        files_under_root: true
        output:
          logstash:
            hosts:
              - "server.local:5044"
        ssl:
          enabled: true
          certificate_authorities:
            - path: /etc/filebeat/filebeatCA.crt
              content: ""
        conf_d:
          - name: program
            paths: ["/var/log/program.log"]
        modules_d:
          program:
            directive1:
              var1: "false"
              var2: "/path/to/log/"
            directive2:
              var1: "true"
```

# ToDo

- Refactor the role for use more flexible variables
- Refactor the role template
